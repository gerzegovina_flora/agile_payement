package com.agile.payment.converter;

import com.agile.payment.entity.payment.CapturePaymentRequest;
import com.agile.payment.entity.FinancialOperation;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class CaptureRequestToTransactionConverter implements Converter<CapturePaymentRequest, FinancialOperation> {
    @Override
    public FinancialOperation convert(CapturePaymentRequest source) {
        FinancialOperation financialOperation = new FinancialOperation();
        financialOperation.setAmount(source.getAmount());
        financialOperation.setLocalDateTime(LocalDateTime.now());
        financialOperation.setTransactionType(source.getTransactionType());
        financialOperation.setPaymentOperation(source.getPaymentOperation());
        return financialOperation;
    }
}

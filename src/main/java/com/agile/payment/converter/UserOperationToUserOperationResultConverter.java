package com.agile.payment.converter;

import com.agile.payment.entity.User;
import com.agile.payment.entity.UserOperationResult;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class UserOperationToUserOperationResultConverter implements Converter<User, UserOperationResult> {
    @Override
    public UserOperationResult convert(User source) {
        UserOperationResult userOperationResult = new UserOperationResult();
        userOperationResult.setCreditAmount(source.getCreditAmount());
        userOperationResult.setDebitAmount(source.getDebitAmount());
        return userOperationResult;
    }
}

package com.agile.payment.repository;

import com.agile.payment.entity.payment.PaymentRequest;
import com.agile.payment.entity.payment.TransactionType;
import com.agile.payment.entity.User;
import com.agile.payment.entity.UserOperationResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Function;

@Repository
public class UserRepositoryImpl implements UserRepository {

    private final UserInstance userInstance = UserInstance.INSTANCE;
    private final Map<TransactionType, BiConsumer<User, PaymentRequest>> operationMap;
    private final Map<TransactionType, Function<User, BigDecimal>> userBalances;
    private final ConversionService conversionService;

    public UserRepositoryImpl(Map<TransactionType, BiConsumer<User, PaymentRequest>> operationMap, ConversionService conversionService, Map<TransactionType, Function<User, BigDecimal>> userBalances) {
        this.operationMap = operationMap;
        this.conversionService = conversionService;
        this.userBalances = userBalances;
    }

    @Override
    public UserOperationResult modifyAccount(PaymentRequest paymentRequest) {
        User user = userInstance.getUser();
        operationMap.get(paymentRequest.getTransactionType()).accept(user, paymentRequest);
        return conversionService.convert(user, UserOperationResult.class);
    }

    @Override
    public BigDecimal getPaymentBalance(TransactionType transactionType) {
        return userBalances.get(transactionType).apply(userInstance.getUser());
    }

    enum UserInstance {
        INSTANCE;

        private final User user = new User();

        public User getUser() {
            return user;
        }
    }
}

package com.agile.payment.repository;

import com.agile.payment.entity.FinancialOperation;

import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public abstract class PaymentRepository {

    private final ReadWriteLock lock = new ReentrantReadWriteLock();

    public void addTransaction(FinancialOperation financialOperation){
        lock.writeLock().lock();
        try {
            addTransactionTemplate(financialOperation);
        } finally {
            lock.writeLock().unlock();
        }
    }

    public List<FinancialOperation> getTransactions(){
        lock.readLock().lock();
        try {
            return getTransactionsTemplate();
        } finally {
            lock.readLock().unlock();
        }
    }

    public FinancialOperation getTransaction(int index){
        lock.readLock().lock();
        try {
            return getTransactionTemplate(index);
        } finally {
            lock.readLock().unlock();
        }
    }

    abstract void addTransactionTemplate(FinancialOperation financialOperation);

    abstract List<FinancialOperation> getTransactionsTemplate();

    abstract FinancialOperation getTransactionTemplate(int index);

}

package com.agile.payment.repository;

import com.agile.payment.entity.FinancialOperation;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class PaymentRepositoryImpl extends PaymentRepository {

    private final List<FinancialOperation> transactions = new ArrayList<>();

    @Override
    public void addTransactionTemplate(FinancialOperation financialOperation) {
        transactions.add(financialOperation);
    }

    @Override
    public List<FinancialOperation> getTransactionsTemplate() {
        return transactions;
    }

    @Override
    public FinancialOperation getTransactionTemplate(int index) {
        return transactions.get(index);
    }
}

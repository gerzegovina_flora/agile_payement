package com.agile.payment.repository;

import com.agile.payment.entity.UserOperationResult;
import com.agile.payment.entity.payment.PaymentRequest;
import com.agile.payment.entity.payment.TransactionType;

import java.math.BigDecimal;

public interface UserRepository {

    UserOperationResult modifyAccount(PaymentRequest paymentRequest);

    BigDecimal getPaymentBalance(TransactionType transactionType);

}

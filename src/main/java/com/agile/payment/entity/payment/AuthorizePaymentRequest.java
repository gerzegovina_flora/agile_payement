package com.agile.payment.entity.payment;

public class AuthorizePaymentRequest extends PaymentRequest {

    private PaymentOperation paymentOperation = PaymentOperation.AUTHORIZATION;

    public PaymentOperation getPaymentOperation() {
        return paymentOperation;
    }
}

package com.agile.payment.entity.payment;

public class CapturePaymentRequest extends PaymentRequest {

    private PaymentOperation paymentOperation = PaymentOperation.CAPTURE;

    public PaymentOperation getPaymentOperation() {
        return paymentOperation;
    }
}

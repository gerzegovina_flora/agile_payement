package com.agile.payment.entity.payment;

public enum PaymentOperation {
    AUTHORIZATION, CAPTURE
}

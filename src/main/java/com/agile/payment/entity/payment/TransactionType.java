package com.agile.payment.entity.payment;

public enum TransactionType {
    DEBIT, CREDIT
}

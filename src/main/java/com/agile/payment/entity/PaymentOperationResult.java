package com.agile.payment.entity;

public class PaymentOperationResult {

    private UserOperationResult userOperationResult;
    private FinancialOperation financialOperation;


    public PaymentOperationResult(UserOperationResult userOperationResult, FinancialOperation financialOperation) {
        this.userOperationResult = userOperationResult;
        this.financialOperation = financialOperation;
    }

    public FinancialOperation getFinancialOperation() {
        return financialOperation;
    }

    public UserOperationResult getUserOperationResult() {
        return userOperationResult;
    }
}

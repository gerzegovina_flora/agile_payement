package com.agile.payment.entity;

import java.math.BigDecimal;

public class UserOperationResult {
    private BigDecimal debitAmount;
    private BigDecimal creditAmount;

    public BigDecimal getDebitAmount() {
        return debitAmount;
    }

    public void setDebitAmount(BigDecimal debitAmount) {
        this.debitAmount = debitAmount;
    }

    public BigDecimal getCreditAmount() {
        return creditAmount;
    }

    public void setCreditAmount(BigDecimal creditAmount) {
        this.creditAmount = creditAmount;
    }
}

package com.agile.payment.entity;

import com.agile.payment.entity.payment.PaymentOperation;
import com.agile.payment.entity.payment.TransactionType;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class FinancialOperation {

    private LocalDateTime localDateTime;
    private TransactionType transactionType;
    private BigDecimal amount;
    private PaymentOperation paymentOperation;
    private String error;

    public LocalDateTime getLocalDateTime() {
        return localDateTime;
    }

    public void setLocalDateTime(LocalDateTime localDateTime) {
        this.localDateTime = localDateTime;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public PaymentOperation getPaymentOperation() {
        return paymentOperation;
    }

    public void setPaymentOperation(PaymentOperation paymentOperation) {
        this.paymentOperation = paymentOperation;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}

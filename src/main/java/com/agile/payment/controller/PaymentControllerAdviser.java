package com.agile.payment.controller;

import com.agile.payment.entity.ErrorMessage;
import com.agile.payment.exception.InvalidAmountOperation;
import com.agile.payment.exception.InvalidDataRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class PaymentControllerAdviser {

    @ExceptionHandler({InvalidAmountOperation.class, InvalidDataRequest.class})
    public final ResponseEntity<ErrorMessage> handleException(Exception ex) {
        ErrorMessage errorMessage = new ErrorMessage();
        errorMessage.setMessage(ex.getMessage());

        return ResponseEntity
                .status(HttpStatus.OK)
                .body(errorMessage);
    }

}

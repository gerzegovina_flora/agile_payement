package com.agile.payment.controller;

import com.agile.payment.entity.PaymentOperationResult;
import com.agile.payment.entity.payment.AuthorizePaymentRequest;
import com.agile.payment.entity.payment.CapturePaymentRequest;
import com.agile.payment.service.PaymentService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PaymentController {

    private final PaymentService paymentService;

    public PaymentController(PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    @RequestMapping(value = "/payment/authorize", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PaymentOperationResult> authorize(@RequestBody AuthorizePaymentRequest authorizePaymentRequest) {
        PaymentOperationResult authorizeResult = paymentService.authorize(authorizePaymentRequest);

        return ResponseEntity
                .status(HttpStatus.OK)
                .body(authorizeResult);
    }

    @RequestMapping(value = "/payment/capture", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PaymentOperationResult> authorize(@RequestBody CapturePaymentRequest capturePaymentRequest) {
        PaymentOperationResult captureResult = paymentService.capture(capturePaymentRequest);

        return ResponseEntity
                .status(HttpStatus.OK)
                .body(captureResult);
    }

}

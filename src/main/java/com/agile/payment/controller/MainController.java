package com.agile.payment.controller;

import com.agile.payment.entity.FinancialOperation;
import com.agile.payment.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller("/")
public class MainController {

    private final PaymentService paymentService;

    public MainController(PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    @RequestMapping
    public String transactionList(Model model) {
        List<FinancialOperation> transactions = paymentService.getTransactions();

        model.addAttribute("transactions", transactions);
        return "transactions";
    }

}

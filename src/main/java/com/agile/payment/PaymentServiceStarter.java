package com.agile.payment;

import com.agile.payment.entity.payment.CapturePaymentRequest;
import com.agile.payment.entity.payment.PaymentRequest;
import com.agile.payment.entity.payment.TransactionType;
import com.agile.payment.entity.User;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Supplier;

@SpringBootApplication
public class PaymentServiceStarter extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(PaymentServiceStarter.class);
        app.run(args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(PaymentServiceStarter.class);
    }

    @Bean
    public Map<TransactionType, BiConsumer<User, PaymentRequest>> operationMap() {
        return new HashMap<TransactionType, BiConsumer<User, PaymentRequest>>() {{
            put(TransactionType.CREDIT, (user, request) -> {
                BigDecimal amount = request instanceof CapturePaymentRequest ? request.getAmount().negate() : request.getAmount();
                user.setCreditAmount(amount);
            });
            put(TransactionType.DEBIT, (user, request) -> {
                BigDecimal amount = request instanceof CapturePaymentRequest ? request.getAmount().negate() : request.getAmount();
                user.setDebitAmount(amount);
            });
        }};
    }

    @Bean
    public Map<TransactionType, Function<User, BigDecimal>> userBalances() {
        return new HashMap<TransactionType, Function<User, BigDecimal>>() {{
            put(TransactionType.CREDIT, User::getCreditAmount);
            put(TransactionType.DEBIT, User::getDebitAmount);
        }};
    }

}

package com.agile.payment.service;

import com.agile.payment.entity.FinancialOperation;
import com.agile.payment.entity.payment.AuthorizePaymentRequest;
import com.agile.payment.entity.payment.CapturePaymentRequest;
import com.agile.payment.entity.PaymentOperationResult;

import java.util.List;

public interface PaymentService {

    PaymentOperationResult authorize(AuthorizePaymentRequest authorizePaymentRequest);

    PaymentOperationResult capture(CapturePaymentRequest capturePaymentRequest);

    List<FinancialOperation> getTransactions();

}

package com.agile.payment.service;

import com.agile.payment.entity.FinancialOperation;
import com.agile.payment.entity.PaymentOperationResult;
import com.agile.payment.entity.UserOperationResult;
import com.agile.payment.entity.payment.AuthorizePaymentRequest;
import com.agile.payment.entity.payment.CapturePaymentRequest;
import com.agile.payment.entity.payment.PaymentRequest;
import com.agile.payment.repository.PaymentRepository;
import com.agile.payment.repository.UserRepository;
import com.agile.payment.service.policy.PaymentAmountPolicy;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaymentServiceImpl implements PaymentService {

    private final UserRepository userRepository;
    private final PaymentRepository paymentRepository;
    private final ConversionService conversionService;
    private final List<PaymentAmountPolicy> paymentAmountPolicy;


    public PaymentServiceImpl(UserRepository userRepository,
                              PaymentRepository paymentRepository,
                              List<PaymentAmountPolicy> paymentAmountPolicy,
                              ConversionService conversionService) {
        this.userRepository = userRepository;
        this.paymentRepository = paymentRepository;
        this.paymentAmountPolicy = paymentAmountPolicy;
        this.conversionService = conversionService;
    }

    @Override
    public PaymentOperationResult authorize(AuthorizePaymentRequest authorizePaymentRequest) {
        checkPolicies(authorizePaymentRequest);

        UserOperationResult userOperationResult = userRepository.modifyAccount(authorizePaymentRequest);
        FinancialOperation financialOperation = conversionService.convert(authorizePaymentRequest, FinancialOperation.class);
        paymentRepository.addTransaction(financialOperation);

        return getPaymentOperationResult(userOperationResult, financialOperation);
    }

    @Override
    public PaymentOperationResult capture(CapturePaymentRequest capturePaymentRequest) {
        checkPolicies(capturePaymentRequest);

        UserOperationResult userOperationResult = userRepository.modifyAccount(capturePaymentRequest);
        FinancialOperation financialOperation = conversionService.convert(capturePaymentRequest, FinancialOperation.class);
        paymentRepository.addTransaction(financialOperation);

        return getPaymentOperationResult(userOperationResult, financialOperation);
    }

    @Override
    public List<FinancialOperation> getTransactions() {
        return paymentRepository.getTransactions();
    }

    private PaymentOperationResult getPaymentOperationResult(UserOperationResult userOperationResult, FinancialOperation financialOperation) {
        return new PaymentOperationResult(userOperationResult, financialOperation);
    }

    private void checkPolicies(PaymentRequest paymentRequest) {
        paymentAmountPolicy.forEach(policy -> {
            policy.checkPolicy(paymentRequest);
        });
    }
}

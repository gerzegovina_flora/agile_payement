package com.agile.payment.service.policy;

import com.agile.payment.entity.payment.PaymentRequest;

public interface PaymentAmountPolicy {

    void checkPolicy(PaymentRequest paymentRequest);

}

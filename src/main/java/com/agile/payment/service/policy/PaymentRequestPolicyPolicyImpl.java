package com.agile.payment.service.policy;

import com.agile.payment.entity.FinancialOperation;
import com.agile.payment.entity.payment.CapturePaymentRequest;
import com.agile.payment.entity.payment.PaymentRequest;
import com.agile.payment.exception.InvalidAmountOperation;
import com.agile.payment.repository.PaymentRepository;
import com.agile.payment.repository.UserRepository;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class PaymentRequestPolicyPolicyImpl implements PaymentAmountPolicy {

    private final UserRepository userRepository;
    private final PaymentRepository paymentRepository;
    private final ConversionService conversionService;

    public PaymentRequestPolicyPolicyImpl(UserRepository userRepository, PaymentRepository paymentRepository, ConversionService conversionService) {
        this.userRepository = userRepository;
        this.paymentRepository = paymentRepository;
        this.conversionService = conversionService;
    }

    @Override
    public void checkPolicy(PaymentRequest paymentRequest) {
        if (isCaptureRequest(paymentRequest)) {
            CapturePaymentRequest request = (CapturePaymentRequest) paymentRequest;

            BigDecimal balance = userRepository.getPaymentBalance(request.getTransactionType());
            long result = balance.longValue() - request.getAmount().longValue();
            if (result < 0) {
                String errorMessage = "Negative amount within the system: user amount: " + balance + ", transaction amount: " + paymentRequest.getAmount();
                FinancialOperation financialOperation = conversionService.convert(paymentRequest, FinancialOperation.class);
                // Sorry for this shit "design", time is up
                if (financialOperation != null) {
                    financialOperation.setError(errorMessage);
                }
                paymentRepository.addTransaction(financialOperation);

                throw new InvalidAmountOperation(errorMessage);
            }
        }
    }

    private boolean isCaptureRequest(PaymentRequest paymentRequest) {
        return paymentRequest instanceof CapturePaymentRequest;
    }
}

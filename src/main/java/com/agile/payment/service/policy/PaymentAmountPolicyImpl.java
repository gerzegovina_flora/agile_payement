package com.agile.payment.service.policy;

import com.agile.payment.entity.FinancialOperation;
import com.agile.payment.entity.payment.PaymentRequest;
import com.agile.payment.exception.InvalidDataRequest;
import com.agile.payment.repository.PaymentRepository;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

@Component
public class PaymentAmountPolicyImpl implements PaymentAmountPolicy {

    private final ConversionService conversionService;
    private final PaymentRepository paymentRepository;

    public PaymentAmountPolicyImpl(ConversionService conversionService, PaymentRepository paymentRepository) {
        this.conversionService = conversionService;
        this.paymentRepository = paymentRepository;
    }

    @Override
    public void checkPolicy(PaymentRequest paymentRequest) {
        if (paymentRequest.getAmount().longValue() <= 0) {
            String errorMessage = "Amount is negative or zero: " + paymentRequest.getAmount();
            FinancialOperation financialOperation = conversionService.convert(paymentRequest, FinancialOperation.class);
            // Sorry for this shit "design", time is up
            if (financialOperation != null) {
                financialOperation.setError(errorMessage);
            }
            paymentRepository.addTransaction(financialOperation);

            throw new InvalidDataRequest(errorMessage);
        }
    }
}

package com.agile.payment.exception;

public class InvalidAmountOperation extends RuntimeException {


    public InvalidAmountOperation(String message) {
        super(message);
    }
}

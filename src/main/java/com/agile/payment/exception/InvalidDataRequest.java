package com.agile.payment.exception;

public class InvalidDataRequest extends RuntimeException {


    public InvalidDataRequest(String message) {
        super(message);
    }
}

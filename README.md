# Payment Service

## Run It:

The following command

```bash
./gradlew bootRun
```

## How to use
There are two endpoints:

Add money to account
```url
/payment/authorize
```

Charge money from account
```url
/payment/capture
```
The reqests can be
```json
{
	"amount": "100",
	"transactionType": "CREDIT"
}
```
```json
{
	"amount": "100",
	"transactionType": "DEBIT"
}
```
If authorization request is used for debit or credit, the money will be added to debit or credit
If capture request is used for debit or credit, the money will be charged from debit or credit

The main page with transactions:
```url
http://localhost:8080/
```

## License
Please, feel free to use. I am not greedy